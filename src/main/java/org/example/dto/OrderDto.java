package org.example.dto;

import org.example.entity.Dish;

import java.util.List;

public class OrderDto {

    private List<Dish> basket;

    private String orderDate;

    private String phone;

    private String address;

    private String sum;

    public List<Dish> getBasket() {
        return basket;
    }

    public void setBasket(List<Dish> basket) {
        this.basket = basket;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }
}
