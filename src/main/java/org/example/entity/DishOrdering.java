package org.example.entity;

import javax.persistence.*;

@Entity
@Table(name = "DISHORDERING")
public class DishOrdering {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "DISH")
    private Integer dish;

    @Column(name = "ORDERING")
    private Integer ordering;

    @Column(name = "COUNT")
    private Integer count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getDish() {
        return dish;
    }

    public void setDish(Integer dish) {
        this.dish = dish;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
