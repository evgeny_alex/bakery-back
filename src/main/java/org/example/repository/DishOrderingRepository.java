package org.example.repository;

import org.example.entity.DishOrdering;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishOrderingRepository extends JpaRepository<DishOrdering, Long> {
}
