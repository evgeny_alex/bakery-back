package org.example.controllers;

import org.example.dto.OrderDto;
import org.example.entity.Delivery;
import org.example.entity.DishOrdering;
import org.example.entity.Ordering;
import org.example.repository.DeliveryRepository;
import org.example.repository.DishOrderingRepository;
import org.example.repository.OrderingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ordering")
public class OrderingController {

    @Autowired
    private OrderingRepository orderingRepository;

    @Autowired
    private DeliveryRepository deliveryRepository;

    @Autowired
    private DishOrderingRepository dishOrderingRepository;

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/create")
    public ResponseEntity<String> create(@RequestBody OrderDto orderDto) throws ParseException {

        // Сохраняем заказ
        List<Ordering> orderingList = orderingRepository.findAll();

        Ordering ordering = new Ordering();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        ordering.setOrderDate(format.parse(orderDto.getOrderDate()));
        ordering.setAmount(new BigDecimal(orderDto.getSum()));
        ordering.setId((long) orderingList.size() + 1);
        ordering.setPhone(orderDto.getPhone());

        orderingRepository.save(ordering);

        // Сохраняем элементы заказа
        Map<Long, Integer> mapCount = new HashMap<>();
        orderDto.getBasket().forEach(item -> {
            if (mapCount.containsKey(item.getId())) {
                mapCount.put(item.getId(), mapCount.get(item.getId()) + 1);
            } else {
                mapCount.put(item.getId(), 1);
            }
        });
        for (Map.Entry<Long, Integer> entry : mapCount.entrySet()) {
            List<DishOrdering> dishOrderingList = dishOrderingRepository.findAll();

            DishOrdering dishOrdering = new DishOrdering();
            dishOrdering.setOrdering(ordering.getId().intValue());
            dishOrdering.setDish(entry.getKey().intValue());
            dishOrdering.setCount(entry.getValue());
            dishOrdering.setId((long) dishOrderingList.size() + 1);

            dishOrderingRepository.save(dishOrdering);
        }

        // Сохраняем информацию о доставке
        List<Delivery> deliveryList = deliveryRepository.findAll();

        Delivery delivery = new Delivery();
        delivery.setDeliveryDate(format.parse(orderDto.getOrderDate()));
        delivery.setOrdering(ordering.getId().intValue());
        delivery.setAddress(orderDto.getAddress());
        delivery.setId((long) deliveryList.size() + 1);

        deliveryRepository.save(delivery);

        return ResponseEntity.ok("Order is created");
    }
}
